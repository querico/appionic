import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { AjustesPage, LoginPage } from '../index.paginas';
import { UbicacionProvider } from '../../providers/ubicacion/ubicacion';
import { UsuarioProvider } from '../../providers/usuario/usuario';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  lat: number;
  lng: number;
  constructor(public navCtrl: NavController, 
              private menuCtrl: MenuController,
              public _ubicacionProv: UbicacionProvider,
              public _usuarioProv:UsuarioProvider) {
    this._ubicacionProv.inicializarChofer();
    this._ubicacionProv.iniciarGeolocalizacion();    
    
    this._ubicacionProv.chofer.valueChanges()
    .subscribe(data => {

      console.log( data );

      });

  }

  mostrarMenu(){
    this.menuCtrl.toggle();
  }
  /*
  Funciones que direccionan a las páginas solicitadas
  */ 
  openAjustes(){
    this.navCtrl.push(AjustesPage);
    this.menuCtrl.close();
  }

  openLogin(){
    this.navCtrl.push(LoginPage);
    this.menuCtrl.close();
  }

  salir(){
    this._ubicacionProv.detenerUbicacion();
    this._usuarioProv.borrarUsuario();
    this.navCtrl.setRoot( HomePage );
  }
}

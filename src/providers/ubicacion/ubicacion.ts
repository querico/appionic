import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';

import { AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';

import { UsuarioProvider } from '../usuario/usuario';
import { Subscription } from 'rxjs';


@Injectable()
export class UbicacionProvider {

  chofer: AngularFirestoreDocument<any>;
  watch: Subscription;
  constructor(private BD: AngularFirestore,
              private geolocation: Geolocation, 
              public _usuarioProv: UsuarioProvider ) {

    //apunta al objeto con todos los cambios 
    //this.chofer = BD.doc(`/usuarios/${_usuarioProv.clave}`);
  }

  inicializarChofer(){
    this.chofer = this.BD.doc(`/usuarios/${this._usuarioProv.clave}`);

  }

  iniciarGeolocalizacion(){
    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude

      this.chofer.update({
        lat: resp.coords.latitude,
        lng: resp.coords.longitude,
        clave: this._usuarioProv.clave
      });
      console.log(resp.coords);

      this.watch = this.geolocation.watchPosition()
      .subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
      this.chofer.update({
        lat: data.coords.latitude,
        lng: data.coords.longitude,
        clave: this._usuarioProv.clave
      });
      });

     }).catch((error) => {
       console.log('Error getting location', error);
     });
     
  }

  detenerUbicacion(){
    try{
      this.watch.unsubscribe();
    }catch(e){
      console.log(JSON.stringify(e));
    }
     
  }
}

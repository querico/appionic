import { Injectable } from '@angular/core';

//saber a que plataforma es: app o web
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subscription } from 'rxjs';



@Injectable()
export class UsuarioProvider {

  clave:string;
  user:any= {};

  private doc: Subscription;

  constructor(private DB: AngularFirestore,
              private platform: Platform,
              private storage: Storage) {
    
  }

  verificaUsuario(clave:string){

    clave.toLocaleLowerCase();

    return new Promise((resolve, reject) => {

   this.doc = this.DB.doc(`/usuarios/${ clave }`).valueChanges()
      .subscribe( data => {
        if (data) {
          //login correcto
          this.clave = clave;
          this.user = data;
          this.guardarStorage();
          resolve(true);
        }else{
          //login incorrecto
          resolve (false);
        }
      })

    });

  }

  //funcion que guardara la clave en el storage

  guardarStorage(){

    if (this.platform.is('cordova')) {
      //celular
      this.storage.set('clave', this.clave);
    }else{
      localStorage.setItem('clave', this.clave);
    }
  }

  cargarStorage(){
    return new Promise( (resolve, reject) =>{
      if (this.platform.is('cordova')) {
        //celular

        this.storage.get('clave').then( val => {
          if(val){
            this.clave = val;
            resolve(true);
          }else{
            resolve(false);
          }
        });

      }else{
        //escritorio
        if(localStorage.getItem('clave')){
          this.clave = localStorage.getItem('clave');
          //existe clave
          resolve(true);
        }else{
          //no existe clave
          resolve(false)
        }
      }
    });
  }

  borrarUsuario(){
    this.clave = null;
    if(this.platform.is('cordova')){
      this.storage.remove('clave');
    }else{
      localStorage.removeItem('clave');
    }
    this.doc.unsubscribe();
  }
}
